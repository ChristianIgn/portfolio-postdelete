const express = require("express");
var morgan = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

const PORT = process.env.PORT;

app.use(cors({ origin: [process.env.REACT_APP_URL, process.env.REACT_APP_URL2]}));

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());
app.use(morgan("tiny"));
app.use(express.json());

//routes
app.use("/test", require("./routes/test"));
app.use("/user", require("./routes/user"));
app.use("/post", require("./routes/post"));
app.use("/interaction", require("./routes/interactions"));

//error 500 handler
app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500;
  console.error(err.message, err.stack);
  res.status(statusCode).json({ message: err.message });
  return;
});

//error 400 handler
app.use((req, res, next) => {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

const server = app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
