const express = require("express");
const router = express.Router();
const { delete_vote_query } = require("../models/interactions/delete_vote.js");
const { fav_query } = require("../models/interactions/user_fav.js");
const { like_query } = require("../models/interactions/like.js");
const { dislike_query } = require("../models/interactions/dislike.js");


router.delete("/vote_delete", (req, res) => {
  const token = req.query.token;
  const data = req.body;
  delete_vote_query(data, token, function (result) {
    if (result.rowCount > 0) {
      res.status(200).json({ message: true });
    } else {
      res.status(404).json({ message: false });
    }
  });
});

router.post("/fav", (req, res) => {
  const token = req.query.token;
  const data = req.body;
  fav_query(data, token, function (result) {
    if (result.rowCount > 0) {
      res.status(200).json({ message: true });
    } else if (result === "Error email user fav") {
      res.status(200).json({ message: result });
    } else {
      res.status(404).json({ message: false });
    }
  });
});

router.post("/like", (req, res) => {
  const token = req.query.token;
  const data = req.body;
  like_query(data, token, function (result) {
    if (result.rowCount > 0) {
      res.status(200).json({ message: true });
    } else {
      res.status(404).json({ message: false });
    }
  });
});

router.post("/dislike", (req, res) => {
  const token = req.query.token;
  const data = req.body;
  dislike_query(data, token, function (result) {
    if (result.rowCount > 0) {
      res.status(200).json({ message: true });
    } else {
      res.status(404).json({ message: false });
    }
  });
});


module.exports = router;
