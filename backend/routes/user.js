const express = require("express");
const router = express.Router();
const { create_user_query } = require("../models/users/create_user.js");
const { login_query } = require("../models/users/login.js");
const { accessToken } = require("../token/token");

router.post("/signup", (req, res) => {
  const data = req.body;
  create_user_query(data, function (result) {
    if (result.rowCount > 0) {
      var token = accessToken(data.email);
      res.status(200).json({ token: token });
    } else {
      res.status(404).json({ message: false });
    }
  });
});

router.post("/login", (req, res) => {
  const data = req.body;
  login_query(data, function (result) {
    if (result.rowCount > 0) {
      var token = accessToken(data.email);
      res.status(200).json({
        token: token
      });
    } else {
        res.status(404).json({message:false});
    }
  });
});

module.exports = router;
