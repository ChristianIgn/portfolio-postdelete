const express = require("express");
const router = express.Router();
const { test_query } = require("../models/test");

//rout to do test
router.get("/", (req, res) => {
  test_query(function (result) {
    console.log(result);
    res.json({ result: result });
  });
});

module.exports = router;
