const express = require("express");
const router = express.Router();
const { create_post_query } = require("../models/posts/create_post.js");
const { read_post_query } = require("../models/posts/read_post.js");
const { update_post_query } = require("../models/posts/update_post.js");

router.post("/create_post", (req, res) => {
  const token = req.query.token;
  const data = req.body;
  create_post_query(data,token, function (result) {
    if (result.rowCount > 0) {
      res.status(200).json({ message: true });
    } else {
      res.status(404).json({ message: false });
    }
  });
});
router.get("/read_post", (req, res) => {
  const token = req.query.token;
  const data = req.body;
  read_post_query(data, token, function (result) {
    if (result.length > 0) {
      res.status(200).json({ message: result });
    } else {
      res.status(404).json({ message: false });
    }
  });
});
router.put("/update_post", (req, res) => {
  const data = req.body;
  update_post_query(data, function (result) {
    if (result.rowCount > 0) {
      res.status(200).json({ message: true });
    } else {
      res.status(404).json({ message: false });
    }
  });
});

module.exports = router;