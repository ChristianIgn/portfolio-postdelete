const jwt = require("jsonwebtoken");

function accessToken(email) {
  const token = jwt.sign({ email }, process.env.JWTOKEN);
  return token;
};

const decodeToken = (token) => {
  try {
    const decode = jwt.verify(token, process.env.JWTOKEN);
    return decode;
  } catch (error) {
    return false;
  }
};

module.exports = { accessToken, decodeToken };