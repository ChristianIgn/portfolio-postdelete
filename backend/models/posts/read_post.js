const pool = require("../../database");
const { decodeToken } = require("../../token/token");

const read_post_query = async (data, token, result) => {
  const email = decodeToken(token).email;
  const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  const isValid = regex.test(email);
  if (isValid) {
    pool.connect(function (err, client, done) {
      if (err) {
        done();
        result(err);
      } else {
        client.query(
          `SELECT users.email, users.username,
            post.id, post.content, post.created_at, post.updated_at,
            post_delete_votes.delete_votes,
            post_dislikes.dislikes,
            post_likes.likes
            FROM users, user_and_post, post, post_delete_votes, post_dislikes,
            post_likes
            WHERE post.deleted='false' OR post.deleted IS NULL
            AND users.email=user_and_post.email
            AND user_and_post.id_post=post.id
            AND post_delete_votes.id_post=post.id
            AND post_dislikes.id_post=post.id
            AND post_likes.id_post=post.id
            ORDER BY post.created_at ASC
            LIMIT 10`,
          function (err, first_query) {
            if (err) {
              done();
              console.log(err);
              result(err);
            } else {
              const final_rows = first_query.rows.map((row) => {
                return client
                  .query(
                    `SELECT
                    user_post_dislike.dislike
                    FROM user_post_dislike
                    WHERE user_post_dislike.email=$1
                    AND user_post_dislike.id_post=$2`,
                    [email, row.id]
                  )
                  .then((dislike_row) => {
                    return client
                      .query(
                        `SELECT
                    user_post_like.likes
                    FROM user_post_like
                    WHERE user_post_like.email=$1
                    AND user_post_like.id_post=$2`,
                        [email, row.id]
                      )
                      .then((like_row) => {
                        return client
                          .query(
                            `SELECT
                    user_delete_vote.delete_vote
                    FROM user_delete_vote
                    WHERE user_delete_vote.email=$1
                    AND user_delete_vote.id_post=$2`,
                            [email, row.id]
                          )
                          .then((delete_vote_row) => {
                            return client
                              .query(
                                `SELECT
                    user_favs.fav
                    FROM user_favs
                    WHERE user_favs.email=$1 AND user_favs.email_fav=$2`,
                                [email,row.email]
                              )
                              .then((user_fav_row) => {
                                return {
                                  ...row,
                                  like: like_row?.rows?.[0]?.likes || false,
                                  dislike:
                                    dislike_row?.rows?.[0]?.dislike || false,
                                  delete_vote:
                                    delete_vote_row?.rows?.[0]?.delete_vote ||
                                    false,
                                  user_fav:
                                    user_fav_row?.rows?.[0]?.fav || false,
                                };
                              })
                              .catch((err) => {
                                done();
                                console.log(err);
                              });
                          })
                          .catch((err) => {
                            done();
                            console.log(err);
                          });
                      })
                      .catch((err) => {
                        done();
                        console.log(err);
                      });
                  })
                  .catch((err) => {
                    done();
                    console.log(err);
                  });
              });
              Promise.all(final_rows)
                .then((returns) => {
                  done();
                  //console.log(final_rows);
                  result(returns);
                })
                .catch((err) => {
                  done();
                  console.log(err);
                  result(err);
                });
            }
          }
        );
      }
    });
  } else {
    result("no token");
  }
};

module.exports = { read_post_query };
