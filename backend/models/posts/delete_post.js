const pool = require("../../database");
const { decodeToken } = require("../../token/token");

const delete_post_query = async (data, result) => {
  const token = decodeToken(data.token);
  const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  const isValid = regex.test(token.email);
  if (isValid) {
    const deleted_at = new Date();

    pool.connect(function (err, client, done) {
      if (err) {
        done();
        result(err);
      } else {
        client
          .query(
            `SELECT email, id_post FROM user_and_post WHERE email=$1 AND id_post=$2`,
            [token.email, data.id]
          )
          .then((match) => {
            if (match?.rowCount > 0) {
              client
                .query(
                  `UPDATE post SET deleted=$1, deleted_at=$2 WHERE id=$3`,
                  [true, deleted_at, data.id]
                )
                .then((returns) => {
                  done();
                  result(returns);
                }).catch((err) =>{
                  done();
                  result(err);
                });
            } else {
              done();
              result(false);
            }
          })
          .catch((err) => {
            done();
            result(err);
          });
      }
    });
  } else {
    result("no valido");
  }
};

module.exports = { delete_post_query };
