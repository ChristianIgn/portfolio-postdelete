const pool = require("../../database");
const { decodeToken } = require("../../token/token");

function create_post_query(data, token, result) {
  const email = decodeToken(token).email;
  const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  const isValid = regex.test(email);
  if (isValid) {
    var created_at = new Date();
    var updated_at = created_at;

    pool.connect(function (err, client, done) {
      if (err) {
        done();
        result(err);
      } else {
        client.query(
          `INSERT INTO post(content,created_at,updated_at) VALUES($1,$2,$3) RETURNING id`,
          [data.content, created_at, updated_at],
          function (err, first_query) {
            if (err) {
              done();
              console.log(err);
              result(err);
            } else {
              client.query(
                `INSERT INTO user_and_post(email,id_post) VALUES($1,$2)`,
                [email, first_query.rows[0].id],
                function (err, second_query) {
                  if (err) {
                    done();
                    console.log(err);
                    result(err);
                  } else {
                    client.query(
                      `INSERT INTO post_likes(id_post,likes) VALUES($1,0);`,
                      [first_query.rows[0].id],
                      function (err, third_query) {
                        if (err) {
                          done();
                          console.log(err);
                          result(err);
                        } else {
                          client.query(
                            `INSERT INTO post_dislikes(id_post,dislikes) VALUES($1,0);`,
                            [first_query.rows[0].id],
                            function (err, four_query) {
                              if (err) {
                                done();
                                console.log(err);
                                result(err);
                              } else {
                                client.query(
                                  `INSERT INTO post_delete_votes(id_post,delete_votes) VALUES($1,0);`,
                                  [first_query.rows[0].id],
                                  function (err, five_query) {
                                    done();
                                    if (err) {
                                      console.log(err);
                                      result(err);
                                    } else {
                                      result(five_query);
                                    }
                                  }
                                );
                              }
                            }
                          );
                        }
                      }
                    );
                  }
                }
              );
            }
          }
        );
      }
    });
  } else {
    result("no valido");
  }
}

module.exports = { create_post_query };
