const pool = require("../../database");
const { decodeToken } = require("../../token/token");

const update_post_query = async (data, result) => {
  const token = decodeToken(data.token);
  const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  const isValid = regex.test(token.email);
  if (isValid) {
    const updated_at = new Date();

    pool.connect(function (err, client, done) {
      if (err) {
        done();
        result(err);
      } else {
        client.query(
          `UPDATE post SET content=$1, updated_at=$2 WHERE id=$3`,
          [data.content, updated_at, data.id],
          function (err, results) {
            done();
            if (err){
              result(err);
            } else {
              result(results);
            }
          }
        );
      }
    });
  } else {
    result("no valido");
  }
};

module.exports = { update_post_query };
