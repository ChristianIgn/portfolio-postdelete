const { ClientBase } = require("pg");
const pool = require("../database");

//funcion test
function test_query(result) {
  pool.connect(function (err, client, done) {
    if (err) {
      done();
      result(err);
    } else {
      client.query(`SELECT * FROM test;`, function (err, results) {
        done();
        if (err) result(err);
        else result(results.rows);
      });
    }
  });
}

module.exports = { test_query };
