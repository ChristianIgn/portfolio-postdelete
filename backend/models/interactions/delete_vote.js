const pool = require("../../database");
const { decodeToken } = require("../../token/token");

function delete_vote_query(data, token, result) {
  const email = decodeToken(token).email;
  const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  const isValid = regex.test(email);
  if (isValid) {
    if (data.email === email) {
      pool.connect(function (err, client, done) {
        if (err) {
          done();
          result(err);
        } else {
          client
            .query(`DELETE FROM post WHERE id=$1`, [data.id])
            .then((row) => {
              done();
              result(row);
            })
            .catch((error) => {
              done();
              result(error);
            });
        }
      });
    } else {
      pool.connect(function (err, client, done) {
        if (err) {
          done();
          result(err);
        } else {
          client
            .query(
              `SELECT delete_vote FROM user_delete_vote
                WHERE email=$1 AND id_post=$2`,
              [email, data.id]
            )
            .then((row) => {
              if (row.rowCount === 0) {
                client
                  .query(
                    `INSERT INTO user_delete_vote(email,id_post,delete_vote) VALUES($1,$2,$3);`,
                    [email, data.id, true]
                  )
                  .then((insert) => {
                    client
                      .query(
                        `UPDATE post_delete_votes SET delete_votes=delete_votes+1 WHERE id_post=$1`,
                        [data.id]
                      )
                      .then((update) => {
                        done();
                        result(update);
                      })
                      .catch((error) => {
                        done();
                        result(error);
                      });
                  })
                  .catch((error) => {
                    done();
                    result(error);
                  });
              } else if (row.rowCount === 1) {
                if (row.rows[0].delete_vote == true) {
                  client
                    .query(
                      `UPDATE user_delete_vote SET delete_vote=$3 WHERE id_post=$1 AND email=$2`,
                      [data.id, email, false]
                    )
                    .then(() => {
                      client
                        .query(
                          `UPDATE post_delete_votes SET delete_votes=delete_votes-1 WHERE id_post=$1`,
                          [data.id]
                        )
                        .then((delete_vote_erased) => {
                          done();
                          result(delete_vote_erased);
                        })
                        .catch((error) => {
                          done();
                          result(error);
                        });
                    })
                    .catch((error) => {
                      done();
                      result(error);
                    });
                } else if (row.rows[0].delete_vote == false) {
                  client
                    .query(
                      `UPDATE user_delete_vote SET delete_vote=$3 WHERE id_post=$1 AND email=$2`,
                      [data.id, email, true]
                    )
                    .then(() => {
                      client
                        .query(
                          `UPDATE post_delete_votes SET delete_votes=delete_votes+1 WHERE id_post=$1`,
                          [data.id]
                        )
                        .then((delete_vote_added) => {
                          done();
                          result(delete_vote_added);
                        })
                        .catch((error) => {
                          done();
                          result(error);
                        });
                    })
                    .catch((error) => {
                      done();
                      result(error);
                    });
                } else {
                  done();
                  result("Error");
                }
              } else {
                done();
                result("Error");
              }
            })
            .catch((error) => {
              done();
              result(error);
            });
        }
      });
    }
  } else {
    result("no valido");
  }
}

module.exports = { delete_vote_query };
