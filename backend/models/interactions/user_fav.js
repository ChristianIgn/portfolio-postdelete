const pool = require("../../database");
const { decodeToken } = require("../../token/token");

function fav_query(data, token, result) {
  const email = decodeToken(token).email;
  const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  const isValid = regex.test(email);
  if (isValid) {
    if (data.email === email) {
      result("Error email user fav");
    } else {
      pool.connect(function (err, client, done) {
        if (err) {
          done();
          result(err);
        } else {
          client
            .query(
              `SELECT user_favs.fav FROM user_favs
              WHERE email=$1 AND email_fav=$2`,
              [email, data.email]
            )
            .then((row) => {
              if (row.rowCount > 0) {
                if (row?.rows?.[0]?.fav == true) {
                  client
                    .query(
                      `UPDATE user_favs SET fav=$3 WHERE email=$1 AND email_fav=$2`,
                      [email, data.email, false]
                    )
                    .then((returns) => {
                      done();
                      result(returns);
                    })
                    .catch((error) => {
                      done();
                      result(error);
                    });
                } else if (row?.rows?.[0]?.fav == false) {
                    client
                      .query(
                        `UPDATE user_favs SET fav=$3 WHERE email=$1 AND email_fav=$2`,
                        [email, data.email, true]
                      )
                      .then((returns) => {
                        done();
                        result(returns);
                      })
                      .catch((error) => {
                        done();
                        result(error);
                      });
                  } else {
                    done();
                    result("Error");
                  }
              } else {
                client
                  .query(
                    `INSERT INTO user_favs(email,email_fav,fav) VALUES($1,$2,$3)`,
                    [email, data.email, true]
                  )
                  .then((returns) => {
                    done();
                    result(returns);
                  })
                  .catch((error) => {
                    done();
                    result(error);
                  });
              }
            })
            .catch((error) => {
              done();
              result(error);
            });
        }
      });
    }
  } else {
    result("no valido");
  }
}

module.exports = { fav_query };
