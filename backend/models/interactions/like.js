const pool = require("../../database");
const { decodeToken } = require("../../token/token");

function like_query(data, token, result) {
  const email = decodeToken(token).email;
  const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  const isValid = regex.test(email);
  if (isValid) {
    pool.connect(function (err, client, done) {
      if (err) {
        done();
        result(err);
      } else {
        client
          .query(
            `SELECT likes FROM user_post_like
                WHERE email=$1 AND id_post=$2`,
            [email, data.id]
          )
          .then((row) => {
            if (row.rowCount === 0) {
              client
                .query(
                  `INSERT INTO user_post_like(email,id_post,likes) VALUES($1,$2,$3);`,
                  [email, data.id, true]
                )
                .then((insert) => {
                  client
                    .query(
                      `UPDATE post_likes SET likes=likes+1 WHERE id_post=$1`,
                      [data.id]
                    )
                    .then((update) => {
                      done();
                      result(update);
                    })
                    .catch((error) => {
                      done();
                      result(error);
                    });
                })
                .catch((error) => {
                  done();
                  result(error);
                });
            } else if (row.rowCount === 1) {
              if (row.rows[0].likes == true) {
                client
                  .query(
                    `UPDATE user_post_like SET likes=$3 WHERE id_post=$1 AND email=$2`,
                    [data.id, email, false]
                  )
                  .then(() => {
                    client
                      .query(
                        `UPDATE post_likes SET likes=likes-1 WHERE id_post=$1`,
                        [data.id]
                      )
                      .then((like_removed) => {
                        done();
                        result(like_removed);
                      })
                      .catch((error) => {
                        done();
                        result(error);
                      });
                  })
                  .catch((error) => {
                    done();
                    result(error);
                  });
              } else if (row.rows[0].likes == false) {
                client
                  .query(
                    `UPDATE user_post_like SET likes=$3 WHERE id_post=$1 AND email=$2`,
                    [data.id, email, true]
                  )
                  .then(() => {
                    client
                      .query(
                        `UPDATE post_likes SET likes=likes+1 WHERE id_post=$1`,
                        [data.id]
                      )
                      .then((like_added) => {
                        done();
                        result(like_added);
                      })
                      .catch((error) => {
                        done();
                        result(error);
                      });
                  })
                  .catch((error) => {
                    done();
                    result(error);
                  });
              } else {
                done();
                result("Error");
              }
            } else {
              done();
              result("Error");
            }
          })
          .catch((error) => {
            done();
            result(error);
          });
      }
    });
  }
}

module.exports = { like_query };
