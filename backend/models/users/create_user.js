const pool = require("../../database");
const CryptoJS = require("crypto-js");

function create_user_query(data,result) {
    var passPhrase = process.env.SECRETPHRASE;

    var decrypted = CryptoJS.AES.decrypt(data.passw, passPhrase);
    var decrypted_string = decrypted.toString(CryptoJS.enc.Utf8);

    var pwHashed = CryptoJS.SHA256(decrypted_string).toString(CryptoJS.enc.Base64);
    const created_at = new Date()
    var updated_at = created_at;

    pool.connect(function(err, client, done) {
        if (err) {
            done();
            result(err);
        } else {
            client.query(`INSERT INTO users(email,passw,username,deleted,created_at,updated_at) VALUES($1,$2,$3,$4,$5,$6)`,
            [data.email,pwHashed,data.username,data.deleted,created_at,updated_at],
            function(err, results) {
                done();
                if (err){
                    console.log(err);
                    result(err);
                } else{
                    result(results);
                }
            });
        };
    });
};

module.exports = { create_user_query };