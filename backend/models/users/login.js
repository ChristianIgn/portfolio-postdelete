const pool = require("../../database");
const CryptoJS = require("crypto-js");

function login_query(data,result) {
    var passPhrase = process.env.SECRETPHRASE;

    var decrypted = CryptoJS.AES.decrypt(data.passw, passPhrase);
    var decrypted_string = decrypted.toString(CryptoJS.enc.Utf8);

    var pwHashed = CryptoJS.SHA256(decrypted_string).toString(CryptoJS.enc.Base64);

    pool.connect(function(err, client, done) {
        if (err) {
            done();
            result(err);
        } else {
            client.query(`SELECT * FROM users WHERE email=$1 AND passw=$2`,
            [data.email,pwHashed],
            function(err, results) {
                done();
                if (err){
                    console.log(err);
                    result(err);
                } else{
                    result(results);
                }
            });
        };
    });
};

module.exports = { login_query };