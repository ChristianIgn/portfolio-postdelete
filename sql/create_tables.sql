create table if not exists test(
  test Text,
  PRIMARY KEY(test)
);

insert into test(test) values('test-data');

create table if not exists users(
  email varchar(250) not null,
  passw varchar(250) not null,
  username varchar(250) not null unique,
  deleted boolean,
  created_at date,
  updated_at date,
  deleted_at date,
  PRIMARY KEY (email)
);

create table if not exists post(
  id serial not null,
  content Text not null,
  deleted boolean,
  created_at date,
  updated_at date,
  deleted_at date,
  PRIMARY KEY (ID)
);

create table if not exists  user_and_post(
  email varchar(250) not null,
  id_post integer not null,
  PRIMARY KEY (email,id_post),
  FOREIGN KEY (id_post) REFERENCES post (id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (email) REFERENCES  users (email) ON DELETE CASCADE ON UPDATE CASCADE
);

create table if not exists  user_favs(
  email varchar(250) not null,
  email_fav varchar(250) not null,
  fav boolean,
  PRIMARY KEY (email, email_fav),
  FOREIGN KEY (email) REFERENCES  users (email) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (email_fav) REFERENCES  users (email) ON DELETE CASCADE ON UPDATE CASCADE
);

create table if not exists  user_post_like(
  email varchar(250) not null,
  id_post integer not null,
  likes boolean,
  PRIMARY KEY (email, id_post),
  FOREIGN KEY (email) REFERENCES  users (email) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (id_post) REFERENCES post (id) ON DELETE CASCADE ON UPDATE CASCADE
);

create table if not exists  user_post_dislike(
  email varchar(250) not null,
  id_post integer not null,
  dislike boolean,
  PRIMARY KEY (email, id_post),
  FOREIGN KEY (email) REFERENCES  users (email) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (id_post) REFERENCES post (id) ON DELETE CASCADE ON UPDATE CASCADE
);

create table if not exists  user_delete_vote(
  email varchar(250),
  id_post integer not null,
  delete_vote boolean,
  PRIMARY KEY (email, id_post),
  FOREIGN KEY (email) REFERENCES  users (email) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (id_post) REFERENCES post (id) ON DELETE CASCADE ON UPDATE CASCADE
);

create table if not exists Post_likes(
  id_post integer,
  likes integer,
  PRIMARY KEY (id_post),
  FOREIGN KEY (id_post) REFERENCES post (id) ON DELETE CASCADE ON UPDATE CASCADE
);

create table if not exists Post_dislikes(
  id_post integer,
  dislikes integer,
  PRIMARY KEY (id_post),
  FOREIGN KEY (id_post) REFERENCES post (id) ON DELETE CASCADE ON UPDATE CASCADE
);

create table if not exists Post_delete_votes(
  id_post integer,
  delete_votes integer,
  PRIMARY KEY (id_post),
  FOREIGN KEY (id_post) REFERENCES post (id) ON DELETE CASCADE ON UPDATE CASCADE
);