.DEFAULT_GOAL := up

.PHONY: up
up:
	@docker-compose up -d

.PHONY: rebuild
rebuild: ## rebuild backend Docker image and wipes volumes
	@docker-compose down --remove-orphans
	@docker-compose build --no-cache
	@docker-compose up -d