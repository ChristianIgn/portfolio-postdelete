import { Routes, Route } from 'react-router-dom';
import	Layout from './Layout/Layout';
import Public from './Public/Public';
import RequireAuth from './features/RequireAuth';
import Main from './views/Main';
import UserSignup from './components/UserSignup';
import UserLogin from './components/UserLogin';
import Navbar from "./components/Navbar";
import Redes from "./components/Redes";
import { Toaster } from "sonner";

function App() {
  return (
    <div className="container mx-auto font-mono mb-20">
      <Navbar />
      
      <Toaster />
      <Routes>
        <Route path="/" element={<Layout />} >
          {/* Public Routes */}
          <Route index element={<Public />} />
          <Route path="redes" element={<Redes />} />
          <Route path="login" element={<UserLogin />} />
          <Route path="signup" element={<UserSignup />} />
          {/* Protected Routes */}
          <Route element={<RequireAuth />}>
            <Route path="postdelete" element={<Main />} />
          </Route>
        </Route>
      </Routes>
    </div>
  );
}

export default App;
