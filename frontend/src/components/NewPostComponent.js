import { useState } from "react";
import { useAddPostMutation } from "../store";
import { HiOutlinePlus } from "react-icons/hi";

function NewPostComponent() {
  const [addPost] = useAddPostMutation();
  const [isOpen, setIsOpen] = useState(false);
  const [form, setForm] = useState({
    content: "",
  });
  const handleClick = () => {
    setIsOpen(!isOpen);
  };
  const handleChange = (event) => {
    setForm({
      ...form,
      [event.target.id]: event.target.value,
    });
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    try {
      let formData = {
        content: form.content,
      };
      addPost(formData)
        .unwrap()
      setIsOpen(!isOpen);
      event.target.reset();
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <div>
      <div>
        <span
          className="gap-4 cursor-pointer flex justify-center items-center border-0 text-black text-2xl font-medium"
          onClick={handleClick}
        >
          <div>Agregar Publicación</div>
          <div>
            <HiOutlinePlus />
          </div>
        </span>
      </div>
      {isOpen && (
        <div className="mt-6 outline outline-offset-0 outline-black p-4">
          <form onSubmit={handleSubmit}>
            <div>
              <label
                className="mt-2 block text-sm font-medium text-black undefined"
                htmlFor="content"
              >
                Añade el contenido de tu publicación:
              </label>
              <div>
                <textarea
                  id="content"
                  type="text"
                  value={form.content}
                  onChange={handleChange}
                  autoComplete="off"
                  className="w-full mt-8 outline outline-offset-0 outline-black"
                />
              </div>
            </div>
            <div className="flex justify-center items-center">
              <button
                type="submit"
                className="m-4 p-2 text-white bg-black outline outline-offset-0 outline-black border-black"
              >
                Publicar
              </button>
            </div>
          </form>
        </div>
      )}
    </div>
  );
}

export default NewPostComponent;
