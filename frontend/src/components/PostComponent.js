import { useState } from "react";

import { useInDeleteVoteMutation } from "../store";
import { useInFavMutation } from "../store";
import { useInLikeMutation } from "../store";
import { useInDislikeMutation } from "../store";

import {
  BsSuitHeart,
  BsSuitHeartFill,
  BsHandThumbsDown,
  BsHandThumbsDownFill,
  BsHandThumbsUpFill,
  BsHandThumbsUp,
  BsXSquareFill,
  BsXSquare,
} from "react-icons/bs";
import { toast } from "sonner";

function PostComponent({
  content,
  created_at,
  delete_vote,
  delete_votes,
  dislike,
  dislikes,
  email,
  id,
  like,
  likes,
  updated_at,
  user_fav,
  username,
}) {
  const [delete_vote_request] = useInDeleteVoteMutation();
  const [fav_request] = useInFavMutation();
  const [like_request] = useInLikeMutation();
  const [dislike_request] = useInDislikeMutation();

  const [isFav] = useState(user_fav);
  const [isLike] = useState(like);
  const [isDeleteVote] = useState(delete_vote);
  const [isDislike] = useState(dislike);

  const handleFavClick = () => {
    try {
      let formData = {
        email: email,
      };
      fav_request(formData).then((res) => {
        if (res.data.message.toString() === "Error email user fav") {
          toast.error("No puedes dar como favorito tu mismo usuario");
        }
      });
    } catch (err) {
      toast.error("Ha ocurrido un error");
    }
  };

  const handleLikeClick = () => {
    try {
      let formData = {
        id: id,
        email: email,
      };
      like_request(formData).unwrap();
    } catch (err) {
      toast.error("Ha ocurrido un error");
    }
  };

  const handleDislikeClick = () => {
    try {
      let formData = {
        id: id,
        email: email,
      };
      dislike_request(formData).unwrap();
    } catch (err) {
      toast.error("Ha ocurrido un error");
    }
  };

  const handleDeleteVoteClick = () => {
    try {
      let formData = {
        id: id,
        email: email,
      };
      delete_vote_request(formData).unwrap();
    } catch (err) {
      toast.error("Ha ocurrido un error");
    }
  };
  return (
    <div className="mt-6">
      <div className="p-2 outline outline-offset-0 outline-black">
        <div className="flex justify-between">
          <div className="flex justify-start items-center gap-2">
            <div className="cursor-pointer">{username}</div>
            <div onClick={handleFavClick} className="cursor-pointer">
              {isFav ? <BsSuitHeartFill /> : <BsSuitHeart />}
            </div>
          </div>
          <div className="flex justify-end  items-center gap-4">
            <div
              onClick={handleLikeClick}
              className="flex cursor-pointer  items-center gap-1"
            >
              <div>{likes} </div>
              <div>{isLike ? <BsHandThumbsUpFill /> : <BsHandThumbsUp />}</div>
            </div>
            <div
              onClick={handleDislikeClick}
              className="flex cursor-pointer  items-center gap-1"
            >
              <div>{dislikes}</div>
              <div>
                {isDislike ? <BsHandThumbsDownFill /> : <BsHandThumbsDown />}
              </div>
            </div>
            <div
              onClick={handleDeleteVoteClick}
              className="flex cursor-pointer  items-center gap-1"
            >
              <div>{delete_votes}</div>
              <div>{isDeleteVote ? <BsXSquareFill /> : <BsXSquare />}</div>
            </div>
          </div>
        </div>
        <div className="p-4 m-6 outline outline-offset-2 outline-black text-lg font-semibold">
          {content}
        </div>
        <div className="flex justify-end  items-center">{created_at}</div>
      </div>
    </div>
  );
}
export default PostComponent;
