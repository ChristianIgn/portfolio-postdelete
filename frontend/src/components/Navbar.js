import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import Button from "./Button";
import logo from "../documents/android-chrome-512x512.png";
import { useEffect, useState } from "react";
import { GrMenu } from "react-icons/gr";
import { SlLogout } from "react-icons/sl";

function Navbar() {
  const [isOpen, setIsOpen] = useState(false);
  const [isLogged, setIsLogged] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    if (localStorage.getItem("token") == null) {
      setIsLogged(false);
    } else {
      setIsLogged(true);
    }
  }, []);

  const handleClick = () => {
    setIsOpen(!isOpen);
  };

  const handleLogout = () => {
    localStorage.removeItem("token");
    setIsLogged(!isLogged);
    navigate("/");
  };
  return (
    <nav>
      <div className="flex flex-wrap items-center justify-between m-4">
        <Link to="/">
          <img className="object-scale-down h-40" src={logo} alt="logo"></img>
        </Link>
        <div className="hidden md:flex">
          <ul className="flex gap-8">
            <li key="PostDelete">
              <Button primary>
                <Link to="/postdelete">PostDelete</Link>
              </Button>
            </li>
            {isLogged ? (
              <>
                <li>
                  <span
                    className="flex gap-3 justify-center items-center w-auto border-0 text-black text-2xl font-medium  hover:underline underline-offset-2"
                    onClick={handleLogout}
                  >
                    Logout <SlLogout />
                  </span>
                </li>
              </>
            ) : (
              <>
                <li key="Registrarse">
                  <Button primary>
                    <Link to="/signup">Registrarse</Link>
                  </Button>
                </li>
                <li key="Login">
                  <Button primary>
                    <Link to="/login">Login</Link>
                  </Button>
                </li>
              </>
            )}
            <li>
              <Button primary>
                <Link to="/redes">Redes</Link>
              </Button>
            </li>
          </ul>
          {/*  mobile menu */}
        </div>
        <div className="md:hidden mt-10">
          <div>
            <span
              className="cursor-pointer border-0 text-black text-2xl font-medium"
              onClick={handleClick}
            >
              <GrMenu />
            </span>
          </div>
          {isOpen && (
            <ul className="md:hidden w-screen mt-6 -ml-4">
              <li key="PostDelete">
                <Button secondary>
                  <Link to="/postdelete">PostDelete</Link>
                </Button>
              </li>
              {isLogged ? (
                <>
                  <li onClick={handleLogout}>
                    <Button secondary>Logout</Button>
                  </li>
                </>
              ) : (
                <>
                  <li key="Registrarse">
                    <Button secondary>
                      <Link to="/signup">Registrarse</Link>
                    </Button>
                  </li>
                  <li key="Login">
                    <Button secondary>
                      <Link to="/login">Login</Link>
                    </Button>
                  </li>
                </>
              )}
              <li>
                <Button secondary>
                  <Link to="/redes">Redes</Link>
                </Button>
              </li>
            </ul>
          )}
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
