import Linkedin from "../documents/linkedin.png";
import gitlab from "../documents/gitlab.png";
import GetOnBrd from "../documents/getonbrd.svg";

function Redes() {
  return (
    <div className="m-6">
      <div className="m-10 md:flex outline outline-offset-0 outline-black">
        <div>
          <a href="https://www.linkedin.com/in/christián-tapia-peñaloza-5822461a5">
            <img  className="outline outline-offset-0 outline-black" width="400px" src={Linkedin} alt="linkedin"></img>
          </a>
        </div>
        <div className="flex items-center justify-center w-full bg-slate-300 text-justify text-black text-2xl font-bold tracking-wider">LinkedIn</div>
      </div>
      <div className="m-10 md:flex outline outline-offset-0 outline-black">
        <div>
          <a href="https://gitlab.com/ChristianIgn/portfolio-postdelete">
            <img className="outline outline-offset-0 outline-black" width="400px" src={gitlab} alt="gitlab"></img>
          </a>
        </div>
        <div className="flex items-center justify-center w-full bg-slate-300 text-justify text-black text-2xl font-bold tracking-wider">Gitlab</div>
      </div>
      <div className="m-10 md:flex outline outline-offset-0 outline-black">
        <div>
          <a href="https://www.getonbrd.com/p/christian-tapia-penaloza">
            <img className="outline outline-offset-0 outline-black" width="400px" src={GetOnBrd} alt="getonbrd"></img>
          </a>
        </div>
        <div className="flex items-center justify-center w-full bg-slate-300 text-justify text-black text-2xl font-bold tracking-wider">Getonbrd</div>
      </div>
    </div>
  );
}
export default Redes;
