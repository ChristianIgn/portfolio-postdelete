import { useFetchTestQuery } from "../store";

function TestComponent() {
  const { data, error, isFetching } = useFetchTestQuery();

  let content;
  if (isFetching) {
    content = <div>Fetching..</div>;
  } else if (error) {
    content = <div>Error loading.</div>;
  } else {
    content = data.result.map((result) => {  
        return <div key={result.test}>{result.test}</div>
    });
  }

  return (
    <div>
      <div className="m-2 flex flex-row items-center justify-between">
        <h3 className="text-lg font-bold">Testing</h3>
      </div>
      <div>{content}</div>
    </div>
  );
}

export default TestComponent;