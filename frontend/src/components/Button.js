import className from 'classnames';

function Button({
  children,
  primary,
  secondary,
  variation,
  ...rest
}) {
  const classes = className(
    rest.className,
    {
      'w-auto border-0 text-black text-2xl font-medium  hover:underline underline-offset-2': primary,
      'w-full outline outline-offset-2 outline-black text-black text-2xl font-medium  hover:underline underline-offset-2 m-1 p-2': secondary
    }
  );

  return (
    <button className={classes}>
      {children}
    </button>
  );
}

Button.propTypes = {
  checkVariationValue: ({ primary, secondary, variation }) => {
    const count =
      Number(!!primary) +
      Number(!!secondary) +
      Number(!!variation)

    if (count > 1) {
      return new Error(
        'Only one of primary, secondary, variation can be true'
      );
    }
  },
};

export default Button;