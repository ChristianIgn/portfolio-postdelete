import { useNavigate, Link } from "react-router-dom";
import CryptoJS from "crypto-js";
import { useLoginUserMutation } from "../store";
import { useState } from "react";
import { toast } from "sonner";

function UserSignup() {
  const [loginUser, results] = useLoginUserMutation();
  const navigate = useNavigate();
  const [form, setForm] = useState({
    email: "",
    passw: "",
  });

  var passPhrase = process.env.REACT_APP_SECRETPHRASE;

  const handleChange = (event) => {
    setForm({
      ...form,
      [event.target.id]: event.target.value,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    try {
      var encrypted_pass = CryptoJS.AES.encrypt(
        form.passw,
        passPhrase
      ).toString();
      let formData = {
        email: form.email,
        passw: encrypted_pass,
      };
      loginUser(formData)
        .unwrap()
        .then((data) => {
          localStorage.setItem("token", data.token);
          navigate("/postdelete");
          navigate(0);
        })
        .catch((error) => {
          toast.error("Ha ocurrido un error");
        });
      event.target.reset();
    } catch (err) {
      toast.error("Ha ocurrido un error");
    }
  };

  return (
    <div>
      <div className="flex flex-col items-center min-h-screen pt-20 sm:justify-center sm:pt-6">
        {results.isLoading ? <h1>Loading</h1> : null}
        <div>
          <a href="/">
            <h3 className="text-4xl font-bold text-black">PostDelete</h3>
          </a>
        </div>
        <div className="outline outline-offset-0 outline-black w-full px-6 py-4 mt-6 overflow-hidden bg-white shadow-md sm:max-w-md">
          <form onSubmit={handleSubmit}>
            <div className="mt-4">
              <label
                htmlFor="email"
                className="block text-sm font-medium text-black undefined"
              >
                Email
              </label>
              <div className="flex flex-col items-start">
                <input
                  id="email"
                  type="email"
                  value={form.email}
                  onChange={handleChange}
                  autoComplete="off"
                  className="block w-full mt-1 outline outline-offset-0 outline-black shadow-sm"
                />
              </div>
            </div>
            <div className="mt-4">
              <label
                htmlFor="password"
                className="block text-sm font-medium text-black undefined"
              >
                Password
              </label>
              <div className="flex flex-col items-start">
                <input
                  id="passw"
                  type="password"
                  value={form.passw}
                  onChange={handleChange}
                  autoComplete="off"
                  className="block w-full mt-1 outline outline-offset-0 outline-black shadow-sm"
                />
              </div>
            </div>
            <div className="flex items-center justify-end mt-4">
              <Link
                className="text-sm text-gray-600 underline"
                to="/signup"
              >
                ¿No tienes una cuenta?
              </Link>
              <button
                type="submit"
                className="inline-flex items-center px-4 py-2 ml-4 text-xs font-semibold tracking-widest text-white uppercase transition duration-150 ease-in-out bg-gray-900 border border-transparent active:bg-gray-900 false"
              >
                Iniciar Sesión
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default UserSignup;
