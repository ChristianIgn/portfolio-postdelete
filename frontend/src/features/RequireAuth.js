import { useLocation, Navigate, Outlet } from "react-router-dom";

function RequireAuth () {
    const token = localStorage.getItem("token");
    const location = useLocation()

    return (
        token
            ? <Outlet />
            : <Navigate to="/signup" state={{ from: location }} replace />
    );
}
export default RequireAuth;