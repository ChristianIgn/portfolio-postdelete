import './index.css';
import React from 'react';
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
import { store } from './store';
import App from './App';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

const el = document.getElementById('root');
const root = createRoot(el);

root.render(
  <Provider store={store}>
    <BrowserRouter>
      <Routes>
        <Route path="/*" element={<App />} />
      </Routes>  
    </BrowserRouter>
  </Provider>
);
