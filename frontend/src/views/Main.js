import { useFetchPostsQuery } from "../store";
import PostComponent from "../components/PostComponent";
import NewPostComponent from "../components/NewPostComponent";

function Main() {
  const { data, error, isFetching } = useFetchPostsQuery();

  let content;
  if (isFetching) {
    content = <div>Fetching..</div>;
  } else if (error) {
    content = <div>No hay datos para mostrar.</div>;
  } else {
    content = data.message?.map((row) => {
      return (
        <PostComponent key={row.id}
          content={row.content}
          created_at={row.created_at.substring(0, 10)}
          delete_vote={row.delete_vote}
          delete_votes={row.delete_votes}
          dislike={row.dislike}
          dislikes={row.dislikes}
          email={row.email}
          id={row.id}
          like={row.like}
          likes={row.likes}
          updated_at={row.updated_at.substring(0, 10)}
          user_fav={row.user_fav}
          username={row.username}
        />
      );
    });
  }
  return (
    <div>
      <NewPostComponent />
      <div>{content}</div>
    </div>
  );
}
export default Main;
