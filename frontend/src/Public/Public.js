import { RiPulseLine } from "react-icons/ri";
import resumeImage from "../documents/resume.JPG";
import resume from "../documents/resume.pdf";
import RF_RNF_image from "../documents/requisitos.JPG";
import RF_RNF from "../documents/RF_RNF_portfolio_postdelete.pdf";
import BPMN_image from "../documents/bpmn.JPG";
import BPMN from "../documents/BPMN_portfolio_postdelete.pdf";
import Modelos_image from "../documents/Modelos.JPG";
import Modelos from "../documents/modelos_portfolio_postdelete.pdf";

function Public() {
  return (
    <>
      <div className="m-6 flex justify-center gap-6 text-justify text-black text-4xl font-bold tracking-wider">
        En desarrollo <RiPulseLine />
      </div>
      <div className="m-6 text-justify text-black text-4xl font-bold tracking-wider">
        ¡Hola!
        <br></br>
        Soy Christián.
      </div>
      <div className="m-6 text-justify text-black text-4xl font-bold tracking-wider">
        Bienvenido a mi portafolio.
      </div>
      <div className="m-6 text-justify text-black font-normal tracking-wider">
        Soy Ingeniero Civil en Informática y Telecomunicaciones. Estoy en
        búsqueda de Trabajo, mis áreas de especialidad son la Ingeniería de
        Software, la programación y el desarrollo web. Este portafolio tiene la
        finalidad de mostrar mis habilidades como desarrollador, a lo largo de
        esta página podrás encontrar los documentos de diseño que fueron
        utilizados durante el proceso.
        <br></br>
        PostDelete es un sitio donde se pueden compartir opiniones en forma de
        publicaciones, inspirado en microbloggings como Twitter, Reddit o
        Mastodon. Abordándolo como portafolio, se trata de un Frontend que
        consume una Api CRUD y que almacena los datos en una base de datos
        relacional (Postgres), todo lo anterior en contenedores con el fin de
        demostrar dominio sobre las tecnologías React, Node, Javascript,
        Postgresql, Docker, ...etc.
        <br></br>
        Eres libre de jugar con el software y comunicarme cualquier detalle.
      </div>

      <div className="mt-10 ml-6 mr-6 flex justify-center text-justify text-black text-2xl font-bold tracking-wider outline outline-offset-0 outline-black">
        Documentos de Diseño
      </div>
      <div className="m-10 flex justify-center text-justify text-black text-xl font-bold tracking-wider">
        Requisitos Funcionales y No funcionales
      </div>
      <div className="m-6 md:flex md:items-center md:justify-center">
        <div className="flex items-center">
          <div className="flex">
            Documento muy útil y necesario para el diseño de un sistema.
          </div>
        </div>
        <div className="m-4">
          <a
            href={RF_RNF}
            className="bg-gray-600 outline outline-offset-0 outline-black"
          >
            <img
              width="auto"
              height="auto"
              className="mr-4 outline outline-offset-0 outline-black"
              src={RF_RNF_image}
              alt="requisitos"
            ></img>
          </a>
        </div>
      </div>

      <div className="m-10 flex justify-center text-justify text-black text-xl font-bold tracking-wider">
        BPMN del sistema
      </div>

      <div className="m-6 md:flex md:items-center md:justify-center">
        <div className="flex items-center">
          <div className="flex">
            Documento que facilita la comunicación con otros equipos de trabajo
            para comprobar o agregar funcionalidades.
          </div>
        </div>
        <div className="m-4">
          <a
            href={BPMN}
            className="bg-gray-600 outline outline-offset-0 outline-black"
          >
            <img
              width="auto"
              height="auto"
              className="mr-4 outline outline-offset-0 outline-black"
              src={BPMN_image}
              alt="bpmn"
            ></img>
          </a>
        </div>
      </div>

      <div className="m-10 flex justify-center text-justify text-black text-xl font-bold tracking-wider">
        Modelos lógicos y físicos
      </div>

      <div className="m-6 md:flex md:items-center md:justify-center">
        <div className="flex items-center">
          <div className="flex">
            Documento para esclarecer aspectos técnicos del sistema, en este caso las funciones que debe
            cumplir la Api CRUD, la estructura de la base de datos y la arquitectura que se utilizará.
          </div>
        </div>
        <div className="m-4">
          <a
            href={Modelos}
            className="bg-gray-600 outline outline-offset-0 outline-black"
          >
            <img
              width="auto"
              height="auto"
              className="mr-4 outline outline-offset-0 outline-black"
              src={Modelos_image}
              alt="modelos"
            ></img>
          </a>
        </div>
      </div>

      <div className="m-10 flex justify-center text-justify text-black text-2xl font-bold tracking-wider outline outline-offset-0 outline-black">
        Currículum
      </div>
      <div className="m-10 flex justify-center">
        <a
          href={resume}
          className="bg-gray-600 outline outline-offset-0 outline-black"
        >
          <img
            width="auto"
            height="auto"
            className="mr-4 outline outline-offset-0 outline-black"
            src={resumeImage}
            alt="resume"
          ></img>
        </a>
      </div>
    </>
  );
}
export default Public;
