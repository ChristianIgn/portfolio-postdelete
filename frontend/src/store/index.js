import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { testApi } from "./apis/testApi";
import { usersApi } from "./apis/userApi";
import { postsApi } from "./apis/postApi";

export const store = configureStore({
  reducer: {
    [testApi.reducerPath]: testApi.reducer,
    [usersApi.reducerPath]: usersApi.reducer,
    [postsApi.reducerPath]: postsApi.reducer,
  },
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware()
      .concat(testApi.middleware)
      .concat(usersApi.middleware)
      .concat(postsApi.middleware);
  },
});

setupListeners(store.dispatch);

export { useFetchTestQuery } from "./apis/testApi";
export { useAddUserMutation } from "./apis/userApi";
export { useLoginUserMutation } from "./apis/userApi";
export { useFetchPostsQuery } from "./apis/postApi";
export { useAddPostMutation } from "./apis/postApi";
export { useInDeleteVoteMutation } from "./apis/interactionApi";
export { useInFavMutation } from "./apis/interactionApi";
export { useInLikeMutation } from "./apis/interactionApi";
export { useInDislikeMutation } from "./apis/interactionApi";
