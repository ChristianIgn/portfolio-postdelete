import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const usersApi = createApi({
  reducerPath: 'users',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_BASE_API_URL,
    fetchFn: async (...args) => {
      return fetch(...args);
    },
  }),
  endpoints(builder) {
    return {
      addUser: builder.mutation({
        query: (payload) => {
          return {
            url: '/user/signup',
            method: 'POST',
            body: payload,
          };
        },
      }),
      loginUser: builder.mutation({
        query: (payload) => {
          return {
            url: '/user/login',
            method: 'POST',
            body: payload,
          };
        },
      }),
    };
  },
});

export const {
  useAddUserMutation,
  useLoginUserMutation,
} = usersApi;
export { usersApi };