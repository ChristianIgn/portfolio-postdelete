import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

const postsApi = createApi({
  reducerPath: "posts",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_BASE_API_URL,
    fetchFn: async (...args) => {
      return fetch(...args);
    },
  }),
  tagTypes: ["Post"],
  endpoints(builder) {
    return {
      fetchPosts: builder.query({
        providesTags: (result, error) => {
          if(typeof result == 'undefined'){
            return [{ type: 'Post' }];
          } else {
            const tags = result.message.map((post) => {
              return { type: 'Post', id: post.id };
            });
            return tags;
          }
        },
        query: () => {
          return {
            url: "/post/read_post",
            params: { token: localStorage.getItem("token") },
            method: "GET",
          };
        },
      }),
      addPost: builder.mutation({
        invalidatesTags: (result, error) => {
          return [{ type: 'Post' }];
        },
        query: (payload) => {
          return {
            url: "/post/create_post",
            method: "POST",
            params: { token: localStorage.getItem("token") },
            body: payload,
          };
        },
      }),
    };
  },
});

export const { useFetchPostsQuery, useAddPostMutation } = postsApi;
export { postsApi };
