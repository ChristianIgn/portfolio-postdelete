import { postsApi } from "./postApi";

const interactionsApi = postsApi.injectEndpoints({
  endpoints(builder) {
    return {
      inDeleteVote: builder.mutation({
        invalidatesTags: (result, error) => {
          return [{ type: "Post" }];
        },
        query: (payload) => {
          return {
            url: "/interaction/vote_delete",
            method: "DELETE",
            params: { token: localStorage.getItem("token") },
            body: payload,
          };
        },
      }),
      inFav: builder.mutation({
        invalidatesTags: (result, error) => {
          return [{ type: "Post" }];
        },
        query: (payload) => {
          return {
            url: "/interaction/fav",
            method: "POST",
            params: { token: localStorage.getItem("token") },
            body: payload,
          };
        },
      }),
      inLike: builder.mutation({
        invalidatesTags: (result, error) => {
          return [{ type: "Post" }];
        },
        query: (payload) => {
          return {
            url: "/interaction/like",
            method: "POST",
            params: { token: localStorage.getItem("token") },
            body: payload,
          };
        },
      }),
      inDislike: builder.mutation({
        invalidatesTags: (result, error) => {
          return [{ type: "Post" }];
        },
        query: (payload) => {
          return {
            url: "/interaction/dislike",
            method: "POST",
            params: { token: localStorage.getItem("token") },
            body: payload,
          };
        },
      }),
    };
  },
});

export const {
  useInDeleteVoteMutation,
  useInFavMutation,
  useInLikeMutation,
  useInDislikeMutation,
} = interactionsApi;
export { interactionsApi };
