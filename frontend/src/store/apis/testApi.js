import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

const testApi = createApi({
  reducerPath: 'tests',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_BASE_API_URL,
    fetchFn: async (...args) => {
      return fetch(...args);
    },
  }),
  endpoints(builder) {
    return {
      fetchTest: builder.query({
        query: () => {
          return {
            url: '/test',
            params: {
            },
            method: 'GET',
          };
        },
      }),
    };
  },
});

export const {
  useFetchTestQuery,
} = testApi;
export { testApi };